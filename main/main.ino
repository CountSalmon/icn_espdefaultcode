#include <ESP8266WebServer.h>
#include <FastLED.h>

#define DATA_PIN    D3
#define NUM_LEDS    30
#define ssid       "TNCAPB37675"
#define password   "HelloWorld1234"

ESP8266WebServer server(80);

int currentRGBValues[3] = {255, 255, 255};
int startRGBValues[3];
int endRGBValues[3];
boolean randomIsActive = false;
boolean totallyRandomIsActive = false;
int currentRandomDelay = 0;
int maxRandomDelay = 1;

//handler function prototypes
void handleLEDCall();
void handleNotFound();
void handleRoot();
void handleRandom();
void handleTotallyRandom();
void handleFromTo();
void handleOff();
void handleTestBlink();
void handleRandomConstant();
String getArgValueByKey(String key);
boolean colorArgsExist();


CRGB leds[NUM_LEDS];

void setup() {
  //enable debugging
  Serial.begin(9600);
  pinMode(BUILTIN_LED, OUTPUT);

  //setup wifi
  //WiFi.mode(WIFI_STAT);
  WiFi.begin(ssid, password);
  Serial.println("");

  //connecting
  Serial.println("connecting...");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(". ");
  }
  Serial.print("local IP: ");
  Serial.println(WiFi.localIP());

  //setup leds
  FastLED.setMaxPowerInVoltsAndMilliamps(5, 1000);
  FastLED.addLeds<WS2812B, DATA_PIN, RGB>(leds, NUM_LEDS);

  //configure server
  server.on("/", handleRoot);
  server.onNotFound(handleNotFound);
  server.on("/led", handleLEDCall);
  server.on("/random", handleRandom);
  server.on("/totally/random", handleTotallyRandom);
  server.on("/random/constant", handleRandomConstant);
  server.on("/from/to", handleFromTo);
  server.on("/off", handleOff);
  server.on("/testblink", handleTestBlink);

  //start server
  server.begin();

  Serial.println("-----------setup finished---------------");
}

void loop() {
  //Serial.println("r: " + (String) currentRGBValues[0] + "   g: <" + (String) currentRGBValues[1] + "   b: " + (String) currentRGBValues[2]);

  if (randomIsActive && currentRandomDelay >= maxRandomDelay) {
    currentRandomDelay = 0;
    currentRGBValues[0] = random(255);
    currentRGBValues[1] = random(255);
    currentRGBValues[2] = random(255);
  }
  currentRandomDelay++;

  for (int i = 0; i < 30; i++) {
    if (totallyRandomIsActive) {
      currentRGBValues[0] = random(255);
      currentRGBValues[1] = random(255);
      currentRGBValues[2] = random(255);
    }
    leds[i] = CRGB(currentRGBValues[0], currentRGBValues[1], currentRGBValues[2]);
    FastLED.show();
  }

  server.handleClient();
}


void handleRoot() {
  String message = "";
  message +=            "<html>";
  message +=              "<head>";
  message +=                  "<title>Root</title>";
  message +=              "</head>";
  message +=              "<body>";
  message +=                   "<h3>List of routes</h3>";
  message +=                   "<ul>";
  message +=                      "<li>/led</li>";
  message +=                      "<li>/random</li>";
  message +=                      "<li>/totally/random</li>";
  message +=                      "<li>/random/constant</li>";
  message +=                      "<li>/off</li>";
  message +=                   "</ul>";
  message +=               "</body>";
  message +=            "</html>";
  server.send(200, "text/html", message);
}

void handleNotFound() {
  server.send(404, "text/plain", "NOT FOUND");
}

void handleLEDCall() {
  randomIsActive = false;
  totallyRandomIsActive = false;

  currentRGBValues[0] = atoi(&getArgValueByKey("green")[0]);
  currentRGBValues[1] = atoi(&getArgValueByKey("red")[0]);
  currentRGBValues[2] = atoi(&getArgValueByKey("blue")[0]);
  String message = "";
  for (int i : currentRGBValues) {
    message += (String) i + "\n";
  }
  server.send(200, "text/plain", message);
}

void handleRandom() {
  randomIsActive = true;
  totallyRandomIsActive = false;
  String delayArg = getArgValueByKey("delay");
  if (delayArg.length() > 0) {
    maxRandomDelay = atoi(&delayArg[0]);
  }
  server.send(200, "text/plain", "random mode is activated");

}

void handleTotallyRandom() {
  randomIsActive = false;
  totallyRandomIsActive = true;
  server.send(200, "text/plain", "totally random mode activated");
}

void handleFromTo() {
  randomIsActive = false;
  totallyRandomIsActive = false;

  server.send(200, "text/plain", "fromto");
}

void handleOff() {
  randomIsActive = false;
  totallyRandomIsActive = false;

  currentRGBValues[0] = 0;
  currentRGBValues[1] = 0;
  currentRGBValues[2] = 0;

  server.send(200, "text/plain", "shut the lights!");
}

void handleRandomConstant() {
  randomIsActive = false;
  totallyRandomIsActive = false;

  currentRGBValues[0] = random(255);
  currentRGBValues[1] = random(255);
  currentRGBValues[2] = random(255);

  server.send(200, "text/plain", "random constant generated");
}

void handleTestBlink() {
  digitalWrite(BUILTIN_LED, LOW);
  delay(1000);
  digitalWrite(BUILTIN_LED, HIGH);
  server.send(200, "text/plain", "tested");
}

String getArgValueByKey(String key) {
  for (int i = 0; i < server.args(); i++) {
    if (server.argName(i).equals(key)) {
      return server.arg(i);
    }
  }
  return "";
}

boolean colorArgsExist() {
  return
    getArgValueByKey("red")
    && getArgValueByKey("green")
    && getArgValueByKey("blue");
}
